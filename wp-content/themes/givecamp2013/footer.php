<?php $options = get_option( 'givecamp_theme_options' ); ?>
</div> <!-- End page-wrapper -->
<footer>
    <div class="container">
      <div class="columns three-wide">

        <div>
        <?php if ( is_active_sidebar( 'footer-sidebar' ) ) : ?>
		<?php dynamic_sidebar( 'footer-sidebar' ); ?>
	<?php else: ?>
          <img src="<?php echo get_template_directory_uri(); ?>/img/logo-cooley.png" alt="A partner with The Thomas M. Cooley Law School Pro Bono Junior Associate Program.">
        <?php endif; ?>
        </div>

        <div>
          <?php
		  $defaults = array(
			'theme_location'  => 'footer-menu',
			'menu'            => '',
			'container'       => false,
			'container_class' => '',
			'container_id'    => '',
			'menu_class'      => 'menu',
			'menu_id'         => '',
			'echo'            => true,
			'fallback_cb'     => 'wp_page_menu',
			'before'          => '',
			'after'           => '',
			'link_before'     => '',
			'link_after'      => '',
			'items_wrap'      => '<ul>%3$s</ul>',
			'depth'           => 0,
			'walker'          => new givecamp_walker_nav_menu
		  );

		  wp_nav_menu( $defaults );
		?>
        </div>

        <div class="social">
            <?php
            if ( is_active_sidebar ( 'social-icons' ) ) //check to see if there's a widget in the footer-sidebar sidebar
            {
                dynamic_sidebar ( 'social-icons' ); //output the widgets in the sidebar
            }
            ?>
        </div>

      </div><!-- .columns -->

    </div><!-- .container -->

    <span class="contact-info">
      Community Legal Services |
      P.O. Box 9678
      Wyoming, Michigan 49509 |
      Telephone: (616) 929-5716
    </span>
  </footer>




  <!-- Load web fonts -->
  <script type="text/javascript">
    WebFontConfig = {
      google: { families: [ 'Open+Sans:400,600,400italic:latin', 'Vollkorn:400,400italic:latin' ] }
    };
    (function() {
      var wf = document.createElement('script');
      wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
        '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
      wf.type = 'text/javascript';
      wf.async = 'true';
      var s = document.getElementsByTagName('script')[0];
      s.parentNode.insertBefore(wf, s);
    })();
  </script>

<?php wp_footer(); ?>
</body>
</html>
