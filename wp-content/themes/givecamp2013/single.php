<?php get_header(); ?>
<div class="container motherlode">
    <main>
        <h1><?php wp_title(); ?></h1>
                <span class="meta">Posted on <?php the_date('m-d-Y', '<i>', '</i>'); ?> by <?php the_author(); ?></span>
        <p>
        <?php if ( have_posts() ): ?>
            <?php while ( have_posts() ): the_post(); ?>
                <?php the_content (  ); ?>
            <?php endwhile; ?>
        <?php endif; ?>
        <div class="post-nav">
            <?php previous_post_link(); ?><br />
            <?php next_post_link(); ?>
        </div>
    </p>
    </main>
    <aside>
        <?php get_sidebar( 'blog' ); ?>
    </aside>
</div>
<div class="cta">
  <div class="container">
    <?php
            if ( is_active_sidebar ( 'home-cta' ) ) //check to see if there's a widget in the home-cta sidebar
            {
                dynamic_sidebar ( 'home-cta' ); //output the widgets in the sidebar
            }
            ?>
  </div>
</div>
<?php get_footer(); ?>