<?php 

// Default home page template

?>

<div class="banner-images slider">

  <div style="background-image: url('upload/blue-bridge.jpg');">
    <div class="caption">
      <div class="container">
		<?php
		if ( shortcode_exists( 'metaslider' ) ) {		//check to see if metaslider shortcode is available
			echo do_shortcode("[metaslider id=147]");	//output the slider
		}
		?>
      </div>
    </div><!-- .caption -->
  </div>

</div><!-- .slider.banner-images -->

<div class="dark cta">
	<div class="container">
		<?php if ( is_active_sidebar( 'home-cta' ) ) {	//check to see if there's a widget in the home-cta sidebar ?>
		<ul id="home-cta-sidebar">
			<?php dynamic_sidebar( 'home-cta' );	//output the widgets in the sidebar ?>
		</ul>
		<?php } ?>
	</div>
</div>

<article>
  <div class="container">

	<?php 
	if ( has_post_thumbnail() ) {	//check to see if the home page has a featured image (post thumbnail)
		the_post_thumbnail();		// output the image
	} 
	the_content();					//output the home page content
	?>

  </div>
</article>


<div class="testimonials slider">

	<?php 
	if ( shortcode_exists( 'quoteRotator' ) ) {		//check to see if quoteRotator shortcode is available
		echo do_shortcode("[quoteRotator]");	//output the quotes
	}
	?>

</div><!-- .testimonials.slider -->

<?php 
$args = array(
	'post_type' => 'post',
	'posts_per-page' => 3
);
	
$home_query = new WP_Query( $args ); //get first 3 posts for output
?>

<aside class="dark">
  <div class="container">
	<h1>CLS News</h1>
	<div class="columns three-wide">  
		<?php
		if ($home_query->have_posts()) :;								//there are posts to output
			while( $home_query->have_posts() : $home_query->the_post();	//while still some posts to output
		?>
				<h2>
					<?php the_title(); 	//output the title ?>
				</h2>
				<figure>
				<?php if (has_post_thumbnail()) {						//if there is a featured image (post thumbnail)
					the_post_thumbnail();								//output the image
				} ?>
					<figcaption>				
						<p>
							<?php the_excerpt();	//output the post excerpt ?>
						</p>
					</figcaption>
				</figure>
			
			<?php endwhile; ?>
		<?php endif; ?>
    </div>
  </div>
</aside>

<?php wp_reset_postdata(); ?>