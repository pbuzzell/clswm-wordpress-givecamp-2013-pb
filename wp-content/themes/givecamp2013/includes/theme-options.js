// JavaScript Document
jQuery(document).ready(function($){
	
	$(".rwd-container").hide();

	$("h3.rwd-toggle").click(function(){
	$(this).toggleClass("active").next().slideToggle("fast");
		return false; //Prevent the browser jump to the link anchor
	});
	$( "#sortable" ).sortable({
        placeholder: "ui-state-highlight",
        serialize: { key: "ordinal"}
    });
    /*$( "#sortable" ).disableSelection();*/

});

jQuery(document).ready(function ($) {
    setTimeout(function () {
        $(".fade").fadeOut("slow", function () {
            $(".fade").remove();
        });

    }, 2000);
    $('.icon_holder').click(function(event){
        var socialNetwork = $(this).attr('alt');
        $('#'+socialNetwork+'_icon').toggle();
        $('#'+socialNetwork).slideToggle();
    });
    var fieldID = null;
    var placeholderID = null;
    $('.upload_button').click(function(event) {
        socialNetwork = $(this).attr('id');
        targetField = '#'+socialNetwork+'_icon';
        tb_show('', 'media-upload.php?type=image&amp;TB_iframe=true');
         return false;
    });

    window.send_to_editor = function(html) {
        jQuery('#'+socialNetwork+'_image_holder').append(html);
        imgurl = jQuery('#'+socialNetwork+'_image_holder img').attr('src');
        jQuery('#'+socialNetwork+'_icon_holder').attr('src',imgurl);
        jQuery(targetField).val(imgurl);
        tb_remove();

    }
    
});