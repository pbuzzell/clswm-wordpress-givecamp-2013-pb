<?php $loop = new WP_Query( array( 'post_type' => 'slides', 'posts_per_page' => 100 ) ); ?>

<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
	<?php
	$imageUrl = get_field ( 'slide_image' );
	$data = get_post_custom();
	?>
	<div style="background-image:url('<?php echo $imageUrl [ 'url' ]; ?>'); width:100%;">
		<div class="caption">
			<div class="container">
				<?php echo $data [ 'slide_caption' ] [ 0 ]; ?>
			</div>
		</div>
	</div>
<?php endwhile; ?>
<?php wp_reset_postdata(); ?>