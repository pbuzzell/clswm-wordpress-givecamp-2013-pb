<?php get_header(); ?>
<div class="container motherlode">
    <main>
        <h1>That page doesn't exist</h1>
        <p>Where you looking for something else?</p>
    </main>

    <aside>
        <p>
            <?php wp_nav_menu( array( 'theme_location' => 'main-menu') ); ?>
        </p>
        <?php if ( is_active_sidebar( 'main-sidebar' ) ) {  //check to see if there's a widget in the home-cta sidebar ?>

            <?php dynamic_sidebar( 'main-sidebar' );    //output the widgets in the sidebar ?>

        <?php } ?>
    </aside>
</div>
<div class="cta">
  <div class="container">
    <?php
            if ( is_active_sidebar ( 'home-cta' ) ) //check to see if there's a widget in the home-cta sidebar
            {
                dynamic_sidebar ( 'home-cta' ); //output the widgets in the sidebar
            }
            ?>
  </div>
</div>
<?php get_footer(); ?>